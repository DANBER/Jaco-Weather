# Jaco-Weather
What is the weather like right now and what will it be like in the next days?

<br>

Setup: Go to www.openweathermap.org and find out the number of your city. Insert it into the `config.json` file.

You don't need an extra api-key for this skill, but sometimes the website will not load completely and you have to ask your request again.

<br>

What **you can learn** in this skill:
* Webscraping (Reading reloaded information from websites)

**Complexity**: Medium

<br>

Build and run skill solely for debugging purposes: \
(Assumes  mqtt-broker already running)

```bash
docker build -t skill_jaco_weather_amd64 - < skills/skills/Jaco-Weather/Containerfile_amd64

docker run --network host --rm \
  --volume `pwd`/skills/skills/Jaco-Weather/:/Jaco-Master/skills/skills/Jaco-Weather/:ro \
  --volume `pwd`/skills/skills/Jaco-Weather/skilldata/:/Jaco-Master/skills/skills/Jaco-Weather/skilldata/ \
  --volume `pwd`/userdata/config/:/Jaco-Master/userdata/config/:ro \
  -it skill_jaco_weather_amd64 python3 /Jaco-Master/skills/skills/Jaco-Weather/action_info.py
```

## Sources
- https://openweathermap.org/
