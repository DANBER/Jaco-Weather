import copy
import datetime
import os
import re

import num2words
from babel.dates import format_date
from bs4 import BeautifulSoup
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from jacolib import assistant

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant

path_facts = filepath + "skilldata/facts.json"
request_running = False
url = "https://openweathermap.org/city/{}"
driver: webdriver.Firefox

regexes_today = {
    "humidity": r"Humidity:<\/span>([0-9]+)%<",
    "windspeed": r"<\/svg> ([0-9]+[.]?[0-9]*)m\/s ",
    "uv_index": r"UV:<\/span>([0-9]+)<",
    "pressure": r"<\/svg>([0-9]+)hPa<",
    "temperature": r'<span class="heading" data[a-z0-9-="]+>([-]?[0-9]+[.]?[0-9]*)°C<\/span><\/div>',
    "temp_felt": r"Feels like ([-]?[0-9]+[.]?[0-9]*)°C",
}

regex_minutes = r"background-color: rgb[a]?\(([0-9]+, [0-9]+, [0-9]+)"

regexes_weekly = {
    "date": r">[a-zA-Z]{3}, ([a-zA-Z]{3} [0-9]+)<\/span><div class=\"day-list-values\"",
    "max_min": r">([-]?[0-9]+) \/ ([-]?[0-9]+)°C<\/span><\/div><span class=\"sub\"",
    "weather": r">([a-z ]+)<\/span><span class=\"chevron-container\"",
}


# ==================================================================================================


def load_weather():
    """Load weather from website. Restarts loading if website was not fully loaded"""

    try:
        driver.get(url)
        try:
            # Wait a bit until that the website can load
            element_present = EC.presence_of_element_located(
                (By.CLASS_NAME, "day-list-values")
            )
            WebDriverWait(driver, timeout=3).until(element_present)
        except TimeoutException as e:
            print("TimeOut Error:", e)
            return None
        soup = BeautifulSoup(driver.page_source, "lxml")
        with open(filepath + "skilldata/dump.html", "w+", encoding="utf-8") as file:
            file.write(str(soup))
    except Exception as e:
        print("LoadWeather Error:", e)
        return None

    weather = {"current": {}, "rain_next_hour": [], "week": {}}

    # Current weather
    current_items = soup.find("div", attrs={"class": "current-container"})
    current_items = str(current_items)
    for reg in regexes_today:
        matches = re.findall(regexes_today[reg], current_items)
        if len(matches) > 0:
            weather["current"][reg] = matches[0]

    # Precipitation forecast next hour
    minute_items = soup.find("ul", attrs={"class": "minutes"})
    minute_items = str(minute_items)
    matches = re.findall(regex_minutes, minute_items)
    matches = [m != "72, 72, 74" for m in matches]
    weather["rain_next_hour"].extend(matches)

    # Weekly forecast
    daily_items = soup.find("div", attrs={"class": "daily-container"})
    for day in daily_items.ul.find_all("li", recursive=False):
        day = str(day)
        this_day = {}

        matches = re.findall(regexes_weekly["max_min"], day)
        if len(matches) > 0:
            this_day["max"] = int(matches[0][0])
            this_day["min"] = int(matches[0][1])

        matches = re.findall(regexes_weekly["weather"], day)
        if len(matches) > 0:
            this_day["weather"] = matches[0]

        matches = re.findall(regexes_weekly["date"], day)
        if len(matches) > 0:
            date = "{} {}".format(matches[0], datetime.datetime.now().year)
            date = datetime.datetime.strptime(date, "%b %d %Y").date()
            weather["week"][str(date)] = this_day

    return weather


# ==================================================================================================


def make_current_weather_text(weather):
    """Convert current weather infos to formatted text"""

    required = {"temperature", "humidity", "windspeed", "temp_felt"}
    if (
        weather is None
        or not required.issubset(set(weather["current"].keys()))
        or len(weather["rain_next_hour"]) < 15
    ):
        print("Received Weather:", weather)
        return assist.get_random_talk("load_error")

    language = assist.get_global_config()["language"]
    text = assist.get_random_talk("current_text")

    temp = num2words.num2words(
        int(float(weather["current"]["temperature"])), lang=language
    )
    humidity = num2words.num2words(
        int(float(weather["current"]["humidity"])), lang=language
    )
    windspeed = num2words.num2words(
        int(float(weather["current"]["windspeed"])), lang=language
    )
    temp_felt = num2words.num2words(
        int(float(weather["current"]["temp_felt"])), lang=language
    )
    text = text.format(temp, humidity, windspeed, temp_felt)

    if "uv_index" in weather["current"]:
        uv_value = int(float(weather["current"]["uv_index"]))
        if uv_value > 8:
            uv_value = assist.get_talks()["uv_index"][str(uv_value)]
            uv_note = assist.get_random_talk("uv_note")
            uv_note = uv_note.format(uv_value)
            text = text + " " + uv_note

    # Special information whether weather changes
    no_rain = all([not v for v in weather["rain_next_hour"]])
    only_rain = all([v for v in weather["rain_next_hour"]])
    if no_rain or only_rain:
        note = assist.get_random_talk("no_changes_next_hour")
    else:
        # Check first 5 min and last 5 min if there is rain
        rain_now = any([v for v in weather["rain_next_hour"][:5]])
        rain_after_hour = any([v for v in weather["rain_next_hour"][-5:]])

        if rain_now and not rain_after_hour:
            # Rain stops in the next hour
            for i, v in enumerate(reversed(weather["rain_next_hour"][5:-5])):
                time_stops = i
                if v is True:
                    break
            note = assist.get_random_talk("rain_stops_next_hour")
            time_stops = len(weather["rain_next_hour"][5:-5]) - time_stops + 5
            note = note.format(num2words.num2words(time_stops, lang=language))

        elif not rain_now and rain_after_hour:
            # Rain starts in the next hour
            for i, v in enumerate(weather["rain_next_hour"][5:-5]):
                time_starts = i
                if v is True:
                    break
            note = assist.get_random_talk("rain_starts_next_hour")
            time_starts = time_starts + 5
            note = note.format(num2words.num2words(time_starts, lang=language))

        elif not rain_now and not rain_after_hour:
            # Short rain period in the next hour
            time_starts = float("inf")
            for i, v in enumerate(weather["rain_next_hour"][5:-5]):
                if v is True and time_starts == float("inf"):
                    time_starts = i
                if v is False:
                    time_stops = i
            note = assist.get_random_talk("rain_between_next_hour")
            duration = num2words.num2words(time_stops - time_starts, lang=language)
            time_starts = num2words.num2words(time_starts, lang=language)
            note = note.format(time_starts, duration)

        else:
            # Detect first rain pause longer than 10 minutes between rain
            min_duration = 10
            time_stops = float("inf")
            for i, v in enumerate(weather["rain_next_hour"][5:-5]):
                if v is False and time_stops == float("inf"):
                    time_stops = i
                if v is True:
                    time_starts = i
                    if time_stops != float("inf"):
                        if time_starts - time_stops > min_duration:
                            break
                        else:
                            # Reset stop time because pause was not long enough
                            time_stops = float("inf")
            if time_stops != float("inf") and time_starts - time_stops > min_duration:
                note = assist.get_random_talk("sun_between_next_hour")
                duration = num2words.num2words(time_starts - time_stops, lang=language)
                time_stops = num2words.num2words(time_stops, lang=language)
                note = note.format(time_stops, duration)
            else:
                note = assist.get_random_talk("no_changes_next_hour")

    text = text + " " + note
    return text


# ==================================================================================================


def convert_weather_type(weather_type):
    types = assist.get_talks()["weather_types"]
    day_weather = ""
    for t in types:
        if t in weather_type:
            day_weather = types[t]
    if day_weather == "":
        # If no translation for the weather type is existing use the original one instead
        day_weather = weather_type
    return day_weather


# ==================================================================================================


def make_day_forecast_text(weather, date: datetime.date):
    """Convert weather infos of a specific day to formatted text"""

    if weather is None:
        return assist.get_random_talk("load_error")

    if date == datetime.datetime.now().date():
        return make_current_weather_text(weather)

    if str(date) not in weather["week"]:
        print("Received Weather:", weather)
        return assist.get_random_talk("date_not_possible")

    required = {"max", "min", "weather"}
    if not required.issubset(set(weather["week"][str(date)].keys())):
        print("Received Weather:", weather)
        return assist.get_random_talk("load_error")

    language = assist.get_global_config()["language"]
    temp_max = num2words.num2words(weather["week"][str(date)]["max"], lang=language)
    temp_min = num2words.num2words(weather["week"][str(date)]["min"], lang=language)
    day_weather = convert_weather_type(weather["week"][str(date)]["weather"])

    if (date - datetime.datetime.now().date()).days == 1:
        text = assist.get_random_talk("tomorrow_forecast")
        text = text.format(day_weather, temp_max, temp_min)

    else:
        # Get local day name
        day = format_date(date, "EEEE", locale=language)
        text = assist.get_random_talk("day_forecast")
        text = text.format(day, day_weather, temp_max, temp_min)

    return text


# ==================================================================================================


def make_week_forecast(weather):
    """Convert weather infos of the upcoming week to formatted text"""

    if weather is None or len(weather["week"].keys()) != 8:
        print("Received Weather:", weather)
        return assist.get_random_talk("load_error")

    for d in weather["week"]:
        required = {"max", "min", "weather"}
        if not required.issubset(set(weather["week"][d].keys())):
            return assist.get_random_talk("load_error")

    week_days = copy.deepcopy(weather["week"])
    week_days.pop(str(datetime.datetime.now().date()))

    language = assist.get_global_config()["language"]
    format_values = []

    # Collect weekday values and min and max temperature days
    min_temp = float("inf")
    max_temp = float("-inf")
    max_day = min_day = ""
    for d in week_days:
        date = datetime.datetime.strptime(d, "%Y-%m-%d").date()
        day = format_date(date, "EEEE", locale=language)
        day_weather = convert_weather_type(weather["week"][str(date)]["weather"])
        format_values.append(day)
        format_values.append(day_weather)

        temp = week_days[d]["max"]
        if temp > max_temp:
            max_temp = temp
            max_day = day
        if temp < min_temp:
            min_temp = temp
            min_day = day

    # Add min and max temperature days
    format_values.append(max_day)
    format_values.append(max_temp)
    format_values.append(min_day)
    format_values.append(min_temp)

    text = assist.get_random_talk("week_forecast")
    text = text.format(*format_values[1:])
    return text


# ==================================================================================================


def callback_weather_now(message):
    """Callback to handle current weather request"""
    global request_running

    if request_running:
        result_sentence = assist.get_random_talk("request_running")
    else:
        weather = load_weather()
        result_sentence = make_current_weather_text(weather)

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_weather_week(message):
    """Callback to handle current weather request"""
    global request_running

    if request_running:
        result_sentence = assist.get_random_talk("request_running")
    else:
        weather = load_weather()
        result_sentence = make_week_forecast(weather)

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_weather_date(message):
    """Callback to handle current weather request"""
    global request_running

    if request_running:
        result_sentence = assist.get_random_talk("request_running")
    else:
        slots = assist.extract_entities(message, "skill_dialogs-date")
        if len(slots) == 1:
            date = slots[0]
            date = datetime.datetime.strptime(date, "%Y-%m-%d").date()

            weather = load_weather()
            result_sentence = make_day_forecast_text(weather, date)
        else:
            result_sentence = assist.get_random_talk("date_not_understood")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def load_webdriver():
    global driver

    # Virtual display is needed for geckodriver
    display = Display(visible=0, size=(800, 600))
    display.start()

    options = Options()
    options.headless = True
    driver = webdriver.Firefox(options=options, log_path="/tmp/geckodriver.log")


# ==================================================================================================


def print_debugging():
    """Collection of statements for debugging"""

    print(url)
    weather = load_weather()
    print(weather)

    # weather = {'current': {'humidity': '93', 'windspeed': '1.5', 'uv_index': '5',
    #   'pressure': '1017', 'temperature': '12', 'temp_felt': '12'}, 'rain_next_hour': [False,
    #     False, False, False, False, False, False, False, False, False, False, False, False,
    #     False, False, False, False, False, False, False, False, False, False, False, False,
    #     False, False, False, False, False, False, False, False, False, False, False, False,
    #     False, False, False, False, False, False, False, False, False, False, False, False,
    #     False, False, False, False, False, False, False, False, False, False, False, False],
    #   'week': {'2020-09-01': {'max': 12, 'min': 10, 'weather': 'few clouds'},
    #     '2020-09-02': {'max': 17, 'min': 9, 'weather': 'light rain'},
    #     '2020-09-03': {'max': 19, 'min': 10, 'weather': 'clear sky'},
    #     '2020-09-04': {'max': 23, 'min': 13, 'weather': 'clear sky'},
    #     '2020-09-05': {'max': 23, 'min': 13, 'weather': 'light rain'},
    #     '2020-09-06': {'max': 17, 'min': 11, 'weather': 'broken clouds'},
    #     '2020-09-07': {'max': 14, 'min': 12, 'weather': 'moderate rain'},
    #     '2020-09-08': {'max': 19, 'min': 12, 'weather': 'broken clouds'}}}

    print(make_current_weather_text(weather))
    date = datetime.datetime.now().date() + datetime.timedelta(days=1)
    print(make_day_forecast_text(weather, date))
    print(make_week_forecast(weather))


# ==================================================================================================


def main():
    global assist, url

    assist = assistant.Assistant(repo_path=filepath)
    load_webdriver()
    url = url.format(assist.get_config()["user"]["city_code"])
    # print_debugging()

    assist.add_topic_callback("weather_now", callback_weather_now)
    assist.add_topic_callback("weather_week", callback_weather_week)
    assist.add_topic_callback("weather_date", callback_weather_date)
    assist.run()


# ==================================================================================================

if __name__ == "__main__":
    main()
