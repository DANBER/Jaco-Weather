## intent:weather_now
- Wie ist das Wetter?
- Wie (warm|kalt) ist es draußen?
- Wird es bald regnen?
- Bleibt es nächste Zeit noch trocken?
- Wie windig ist es gerade?

## intent:weather_week
- Gib mir eine Wettervorhersage
- Wetterbericht bitte
- Wie wird das Wetter?
- Wird es nächste Woche schön?
- Ist nächste Woche schlechtes Wetter?
- Wie wird das Wetter in den nächsten Tage?
- Gib mir einen Bericht über das Wetter der nächsten Tage

## intent:weather_date
- Wie wird das Wetter am [Montag](skill_dialogs-date)
- Wird es [morgen](skill_dialogs-date) gutes Wetter?
- Wie ist das Wetter [nächsten Samstag](skill_dialogs-date)?
- Wird es [übermorgen](skill_dialogs-date) regnen?
- Erzähl mir die Wettervorschau für [Freitag](skill_dialogs-date)
- Wie ist das Wetter [in drei Tagen](skill_dialogs-date)?
