## intent:weather_now
- What is the weather like?
- How (warm|cold) is it outside?
- Will it rain soon?
- Will it stay dry for a while?
- How windy is it right now?

## intent:weather_week
- Give me a weather forecast
- Weather forecast please
- How will the weather be (the next days|)?
- Will it be (nice|bad weather) next week?
- Give me a report about the weather in the next days

## intent:weather_date
- How will the weather be on [Monday](skill_dialogs-date)
- Will [tomorrow](skill_dialogs-date) be good weather?
- How is the weather [next Saturday](skill_dialogs-date)?
- Will it rain [the day after tomorrow](skill_dialogs-date)?
- Tell me the weather forecast for [Friday](skill_dialogs-date)
- How will the weather be [in three days](skill_dialogs-date)?
